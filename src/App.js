import React, {useState} from 'react';
import Hero from "./components/Hero";
import { BrowserRouter as Router} from 'react-router-dom';
import { GlobalStyle } from './GlobalStyle';
import Products from "./components/Products";
import { productData } from "./components/Products/data";
import Feature from "./components/Feature";
import Footer from "./components/Footer";

function App() {
    const [ cart, setCart ] = useState([]);

    const addProductToCart = (product) => {
        setCart([...cart, product]);
    }

    const deleteCart = () => {
        setCart([]);
    }

    return (
    <Router>
     <GlobalStyle />
     <Hero cart={cart} deleteCart={deleteCart} />
     <Products data={productData} cart={cart} addProductToCart={addProductToCart} />
     <Feature />
     <Footer />
    </Router>
  );
}

export default App;
