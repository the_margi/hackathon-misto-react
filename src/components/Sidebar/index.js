import React from 'react'
import { SidebarContainer, Icon, CloseIcon, SidebarMenu, OrderNowBtn, SideBtnWrap, ProductCard, ProductImg, ClearCartBtn } from "./Sidebar";
import {render} from "react-dom";

const Sidebar = ({isOpen, toggle, cart, deleteCart}) => {
    const calculateTotal = (cart) => {
        let total = 0;
        if (cart.constructor === Array) {
            cart.forEach(product => {
                total += product.price * product.count;
            })
        }
        return total;
    };

  return (
    <SidebarContainer isOpen={isOpen} onClick={toggle}>
      <Icon onClick={toggle}>
        <CloseIcon />
      </Icon>
      <SidebarMenu>
        <h1>Your Cart:</h1>
          {   cart.map(product => {
              return (
                 <ProductCard key={product.id}>
                     <p>{product.count + " x " + product.name}</p>
                     <ProductImg src={product.img} alt={product.alt}></ProductImg>
                 </ProductCard>
              );
          })}
      </SidebarMenu>
      <SideBtnWrap>
        <OrderNowBtn>Order Now; Total: ${calculateTotal(cart)}</OrderNowBtn>
        <ClearCartBtn onClick={deleteCart}>Clear cart</ClearCartBtn>
      </SideBtnWrap>
    </SidebarContainer>
  );
};

export default Sidebar;
