import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FaTimes } from 'react-icons/fa';

export const SidebarContainer = styled.aside`
  position: fixed;
  z-index: 999;
  width: 350px;
  height: 100%;
  background: #ffc500;
  display: grid;
  align-items: center;
  top: 0;
  transition: 0.3s ease-in-out;
  right: ${({ isOpen }) => ( isOpen ? '0' : '-1000px') };

  @media screen and (max-width: 768px) {
    width: 200px;
  }
  
  @media screen and (max-width: 500px) {
    width: 100%;
  }
`;

export const CloseIcon = styled(FaTimes)`
  color: #000;
`;

export const Icon = styled.div`
  position: absolute;
  top: 1.2rem;
  right: 1.5rem;
  background: transparent;
  border: transparent;
  font-size: 2rem;
  cursor: pointer;
  outline: none;
`;

export const SidebarMenu = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  text-align: center;
  
  h1 {
    font-size: 40px;
  }
`;

export const SideBtnWrap = styled.div`
  display: grid;
  grid-template-columns: 15rem;
  row-gap: 25%;
  justify-content: center;
`;

export const OrderNowBtn = styled.button`
  background: #e31837;
  white-space: normal;
  padding: 16px 64px;
  color: #fff;
  font-size: 16px;
  outline: none;
  border: none;
  cursor: pointer;
  transition: 0.2s ease-in-out;

  &:hover {
    transition: 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;

export const ProductCard = styled.div`
    display: flex;
    position: relative;
    max-height: 100px;
    max-width: 400px;
    justify-content: center;
    padding: 1rem;
  
    p {
      display: inline;
      font-size: 22px;
    }
`;

export const ProductImg = styled.img`
  height: 35px;
  min-width: 35px;
  max-width: 100%;
  object-fit: cover;
`;

export const ClearCartBtn = styled.button`
  background: #e31837;
  white-space: nowrap;
  padding: 16px 64px;
  color: #fff;
  font-size: 16px;
  outline: none;
  border: none;
  cursor: pointer;
  transition: 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;
