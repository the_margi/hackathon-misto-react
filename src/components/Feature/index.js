import React from 'react'
import { FeatureContainer } from './Feature';

const Feature = () => {
  return (
    <FeatureContainer>
      <h1>Pizza of the Day</h1>
      <p>Truffle alfredo sauce topped with 24 carat gold dust</p>
    </FeatureContainer>
  );
};

export default Feature;
