import React from 'react'
import { ProductsContainer, ProductWrapper, ProductsHeading, ProductInfo, ProductTitle, ProductCard, ProductImg, ProductDesc, ProductPrice, ProductButton } from "./Products";

const Products = ({ data, cart, addProductToCart, deleteCart }) => {
  return (
    <ProductsContainer>
      <ProductsHeading>Menu</ProductsHeading>
      <ProductWrapper>
        {data.map(product => {
          return (
            <ProductCard key={product.id}>
              <ProductImg src={product.img} alt={product.alt} />
              <ProductInfo>
                <ProductTitle>{product.name}</ProductTitle>
                <ProductDesc>{product.desc}</ProductDesc>
                <ProductPrice>${product.price}</ProductPrice>
              </ProductInfo>
              <ProductButton onClick={() => {
                  if (cart.includes(product)) {
                      for (let i = 0; i < cart.length; i++) {
                          if (cart[i] === product)
                              cart[i].count += product.count;
                      }
                  } else {
                      addProductToCart(product);
                  }
                  }}>Add to Cart
              </ProductButton>
            </ProductCard>
          )
        })}
      </ProductWrapper>
    </ProductsContainer>
  );
};

export default Products;
