import React, {useRef, useState} from 'react';
import Navbar from "../Navbar";
import Sidebar from "../Sidebar";
import { HeroContainer, HeroContent, HeroItems, HeroH1, HeroP, HeroBtn } from "./Hero";

const Hero = ({cart, deleteCart}) => {
  const [ isMenuOpen, setIsMenuOpen ] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <HeroContainer>
      <Navbar toggle={toggleMenu}/>
      <Sidebar isOpen={isMenuOpen} toggle={toggleMenu} cart={cart} deleteCart={deleteCart}/>
      <HeroContent>
        <HeroItems>
          <HeroH1>Greatest Pizza Ever</HeroH1>
          <HeroP>Ready in 10 minutes</HeroP>
          <HeroBtn>View Menu</HeroBtn>
        </HeroItems>
      </HeroContent>
    </HeroContainer>
  );
};

export default Hero;
